//
//  ViewController.swift
//  Kalender
//
//  Created by Ihwan on 02/04/21.
//

import UIKit
import FSCalendar

class ViewController: UIViewController, FSCalendarDataSource, FSCalendarDelegate, FSCalendarDelegateAppearance {
    
    @IBOutlet weak var calendar: FSCalendar!
    
    fileprivate let formatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        return formatter }()
    
    var arrayOfEvent1 : [String] = ["2021-04-05","2021-04-17"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        calendar.register(CustomCalendarCell.self, forCellReuseIdentifier: "cell")
        calendar.dataSource = self
        calendar.delegate = self
        calendar.placeholderType = .none
        calendar.appearance.subtitleColors
        
    }
    
    func calendar(_ calendar: FSCalendar, cellFor date: Date, at position: FSCalendarMonthPosition) -> FSCalendarCell {
        return calendar.dequeueReusableCell(withIdentifier: "cell", for: date, at: position)
    }
    
   
    func calendar(_ calendar: FSCalendar, subtitleFor date: Date) -> String? {
        let dateString =  self.formatter.string(from:date)
        return self.arrayOfEvent1.contains(dateString) ? "sold out" : nil
    }
   
    func calendar(_ calendar: FSCalendar, appearance: FSCalendarAppearance, subtitleDefaultColorFor date: Date) -> UIColor? {
        let dateString =  self.formatter.string(from:date)
        return self.arrayOfEvent1.contains(dateString) ? .red : nil
    }
}


extension UIView {
    var width: CGFloat {
        return frame.size.width
    }
    var height: CGFloat {
        return frame.size.height
    }
    var left: CGFloat {
        return frame.origin.x
    }
    var right: CGFloat {
        return left + width
    }
    var top: CGFloat {
        return frame.origin.y
    }
    var bottom: CGFloat {
        return top + height
    }
}


class CustomCalendarCell: FSCalendarCell {
    
    private var eventDotSize: CGFloat = 4.0
    private lazy var newDotView: UIView = {
        let view = UIView()
        return view
    }()
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        eventIndicator.subviews.first?.alpha = 0.0 // hide dots of library
        
        let newDotRect = CGRect(x: (frame.width - eventDotSize) / 2,
                                y: eventDotSize / 2,
                                width: eventDotSize,
                                height: eventDotSize)

        newDotView.frame = newDotRect
        newDotView.backgroundColor = .red // {You want}
        newDotView.layer.cornerRadius = eventDotSize / 2
        eventIndicator.addSubview(newDotView)
    }
}
